(function () {
    let delayImgs = [...document.getElementsByClassName('delayImg')]

    let ob = new IntersectionObserver(el => {
        el.forEach(item => {
            if (item.isIntersecting) {
                if (item.target.hasAttribute('data-src')) {
                    item.target.src = item.target.dataset.src
                }
                ob.unobserve(item.target)
            }
        })
    })
    
    delayImgs.forEach(item => {
        ob.observe(item)
    })
    
    let testSwiper = new Swiper('.swiper_container', {
        autoHeight: true,
        allowTouchMove: false
    })
    document.querySelectorAll(".swiper_container .swiper-wrapper li").forEach(item => {
        item.addEventListener("click", event => {
          if(event.target.className == "answer_end") {
            event.target.style.backgroundColor = "#d4a20b"
            testSwiper.slideNext();
            setTimeout(() => {
                testSwiper.slideNext()
            }, 1200)
            if (event.target.hasAttribute("data-a")) {
                document.getElementById(
                    "num"
                ).innerHTML = event.target.getAttribute("data-a");
            }
          }else{
            event.target.style.backgroundColor = "#d4a20b"
            testSwiper.slideNext();
          }
        });
    });

    let startTestSwiper = new Swiper('.swiper-container', {
        autoHeight: true,
        allowTouchMove: false
    })

    document.querySelectorAll(".swiper-container .swiper-wrapper li").forEach(item => {
        item.addEventListener("click", event => {
          if(event.target.className == "answer_end") {
            event.target.style.backgroundColor = "#d4a20b"
            startTestSwiper.slideNext();
            setTimeout(() => {
              startTestSwiper.slideNext()
                $('.test_content_title').hide();
            }, 1200)
            if (event.target.hasAttribute("data-b")) {
                document.getElementById(
                    "num2"
                ).innerHTML = event.target.getAttribute("data-b");
              }
          }else{
            event.target.style.backgroundColor = "#d4a20b"
            startTestSwiper.slideNext();
          }
        });
    });
    $('.close').click(() => {
      $('.start_test').fadeOut().css({"touch-action" : "auto"});
    })

    // document.getElementsByClassName("close")[0].addEventListener('click', ()=>{
    //   document.getElementsByClassName('start_test')[0].style.display="none"
    // })
})()
